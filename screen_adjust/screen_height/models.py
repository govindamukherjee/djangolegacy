from django.db import models
from django.utils import timezone

class CalDB(models.Model):
    myDt = models.DateTimeField(editable=False, unique=True)
    myNum = models.FloatField(unique=False)
    myChar = models.CharField(max_length=8000, unique=False)

    def save(self, *args, **kwargs):
        '''On save, update timestamp'''
        if(not self.id):
            self.myDt = timezone.now()
        return super(CalDB, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.myDt) 