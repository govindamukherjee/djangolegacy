let myScreenWidth = 0;
let myScreenHeight = 0;
let dateString = 0;
let minDistanceMarker = 30; //meter


//-----------------------------------------get python data from view.py in the form of json format via html.
let frlData = JSON.parse(document.getElementById('frl_data').textContent);
console.log("------------------frlData: " + frlData.minDist + " - " + frlData.screenHeight);

minDistanceMarker = frlData.minDist;

const bodydiv = document.getElementById("bodyDiv");
let bodyDivWidth = 0;
let bodyDivHeight = 0;
//---------------------------------------view.py data

//------------------------------------------resize the body div declared in base.html on change in screen size.
// const bodyResizeObserver = new ResizeObserver(entries => {
function baseBodyResize(){
    myScreenWidth = window.innerWidth;   //969px
    myScreenHeight = window.innerHeight; //1585px
    console.log("window width: " + myScreenWidth + " height: " + myScreenHeight);

    // bodydiv.style.height = Math.ceil((myScreenHeight*98.55)/100.00) + "px";    //"955px"
    bodydiv.style.height = frlData.screenHeight + "px";

    bodyDivWidth = bodydiv.offsetWidth;
    bodyDivHeight = bodydiv.offsetHeight;
    console.log("bodyDiv: " + bodyDivWidth + " " + bodyDivHeight); 
}
