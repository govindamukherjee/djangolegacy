const navbar = document.getElementById("navBar");
let navBarWidth = navbar.offsetWidth;
let navBarHeight = navbar.offsetHeight;  //66px
const mapdiv = document.getElementById("mapDiv");
let mapDivWidth = mapdiv.offsetWidth;     
let mapDivHeight = mapdiv.offsetHeight;
const spectrumdiv = document.getElementById("spectrumDiv");
let spectrumDivWidth = spectrumdiv.offsetWidth;
let spectrumDivHeight = spectrumdiv.offsetHeight;
// const middiv = document.getElementById("midDiv");
const topdiv = document.getElementById("topDiv");
let topDivWidth = topdiv.offsetWidth;
let topDivHeight = topdiv.offsetHeight;
const datadiv = document.getElementById("dataDiv");
let dataDivWidth = datadiv.offsetWidth;
let dataDivHeight = datadiv.offsetHeight;
const footerbar = document.getElementById("footerBar");
let footerBarWidth = footerbar.offsetWidth;
let footerBarHeight = footerbar.offsetHeight; //56px


// const resizeObserver = new ResizeObserver(entries => {
function layer01DivResize(){
    document.getElementById("screenHeight").innerHTML = bodyDivHeight;
    document.getElementById("minDistance").innerHTML = minDistanceMarker;

    navBarWidth = navbar.offsetWidth;
    navBarHeight = navbar.offsetHeight;  //66px
    console.log("navBar: " + navBarWidth + " " + navBarHeight);

    footerBarWidth = footerbar.offsetWidth;
    footerBarHeight = footerbar.offsetHeight; //56px
    console.log("footerBar: " + footerBarWidth + " " + footerBarHeight);

    datadiv.style.height = "0px"; //80px

    dataDivWidth = datadiv.offsetWidth;
    dataDivHeight = datadiv.offsetHeight;
    console.log("dataDiv: " + dataDivWidth + " " + dataDivHeight);


    // mapdiv.style.height= Math.ceil((bodyDivHeight*81.98)/100) + "px"; //"783px"
    mapdiv.style.height = (bodyDivHeight - navBarHeight - footerBarHeight - dataDivHeight) + "px"; 

    mapDivWidth = mapdiv.offsetWidth;     
    mapDivHeight = mapdiv.offsetHeight;
    console.log("mapDiv: " + mapDivWidth + " " + mapDivHeight);

    spectrumDivWidth = spectrumdiv.offsetWidth;

    // spectrumdiv.style.height= Math.ceil((bodyDivHeight*41)/100) + "px"; //"392px"
    spectrumdiv.style.height= ((spectrumDivWidth/2) + 10) + "px";

    spectrumDivHeight = spectrumdiv.offsetHeight;
    console.log("spectrumDiv: " + spectrumDivWidth + " " + spectrumDivHeight);

    // middiv.style.height= Math.ceil((bodyDivHeight*10.15)/100) + "px"; //"97px"

    // const midDivWidth = middiv.offsetWidth;
    // const midDivHeight = middiv.offsetHeight;
    // console.log("midDiv: " + midDivWidth + " " + midDivHeight);

    // topdiv.style.height= Math.ceil((bodyDivHeight*41)/100) + "px"; //"392px"
    topdiv.style.height= (mapDivHeight - spectrumDivHeight) + "px";

    topDivWidth = topdiv.offsetWidth;
    topDivHeight = topdiv.offsetHeight;
    console.log("topDiv: " + topDivWidth + " " + topDivHeight);

}


//--------------------------------------------date time update
function addZero(data){
	if(data < 10){
		data = '0' + data;
	}
	return data;
}

function getDT(){
    var today = new Date();
    var yr = addZero(today.getFullYear() - 2000);
    var mon = addZero(today.getMonth() + 1);
    dateString = addZero(today.getFullYear() - 2000) + '/' + addZero(today.getMonth() + 1) + '/' + addZero(today.getDate()) + ' ' + addZero(today.getHours())+ ':' + addZero(today.getMinutes())+ ':' + addZero(today.getSeconds());
    // console.log(dateString);
    return dateString;
}

function dtUpdate()
{
    document.getElementById("dt").innerHTML = "" + getDT();
}

setInterval(dtUpdate, 1000);
//--------------------------------------------date time update

