from django.apps import AppConfig


class ScreenHeightConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'screen_height'
