from django.shortcuts import render
from django.core.files import File
from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from screen_height.models import CalDB
import random

import json

myFilePath = './finalCal.txt'
calString = {"screenHeight": 0, "minDist": 30,}

def saveInDB():
    rnd = round((random.random() * 100), 2)
    try:
        CalDB.objects.get_or_create(
            myNum = rnd,
            myChar = "gogo: " + str(rnd)
        )
        print("saveInDB success")
    except Exception as e:
        print("saveInDB error: ", e)

saveInDB()

def loadFromDB():
    try:
        myQuerry = "SELECT * FROM screen_height_caldb"
        myData = CalDB.objects.raw(myQuerry)
        for c in myData:
            print(c.myChar)
    except Exception as e:
        print("loadFromDB Error: ", e) 

loadFromDB()



def loadCalString():
    global calString
    try:
        with open(myFilePath, 'r') as f:
            testfile = File(f)
            temp = json.loads(testfile.readline())
            calString["screenHeight"] = temp["screenHeight"]
            calString["minDist"] = temp["minDist"]
            print("readline ", calString)
    except Exception as e:
        print("loadCalString error: ", e)

loadCalString()

def indexView(request):
    global calString
    context = {"frlData": calString,}
    print("context: ", context)
    return render(request, 'screen_height/index.html', context)

def htmlToViewUrl(request):
    global calString, comStringLength
    myCall = "myCall" 
    myValue = "myValue"
    myResult = "myResult"
    print("Data received from html")
    if(request.method == 'POST'):
        myCall = request.POST.get('calledFrom')
        myValue = request.POST.get('htmlToViewsValue')
        myResult = "True"
        print(myCall + " : " + myValue + " : " + myResult)
        if(myCall == "screenHeightLimit"):
            print("screen height file writing ready")
            try:
                with open(myFilePath, 'w') as f:
                    testfile = File(f)
                    calString["screenHeight"] = int(myValue)
                    temp = json.dumps(calString)
                    print(temp)
                    testfile.write(temp)
            except Exception as e:
                print("htmlToViewUrl screenHeight error: ", e)
        elif(myCall == "minDistanceLimit"):
            try:
                with open(myFilePath, 'w') as f:
                    testfile = File(f)
                    calString["minDist"] = float(myValue)
                    temp = json.dumps(calString)
                    print(temp)
                    testfile.write(temp)
            except Exception as e:
                print("htmlToViewUrl minDistance error: ", e)

    data = {"Call": myCall, "Value": myValue, "Result": myResult}
    return JsonResponse(data, safe=False)
